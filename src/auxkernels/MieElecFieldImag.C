/// * @file   MieElecFieldImag.C
/// * @author J. Mangeri <john.mangeri@uconn.edu>
/// 
///   This file implements the solution of Mie (1908) for spherical 
///   scattering centers. We use the common nh formulation. 

#include "MieElecFieldImag.h"
#include "FerretConfig.h"
#include <complex>
#include <cmath>

#ifdef FERRET_HAVE_BOOST_MATH_SPECIAL_FUNCTIONS
# include <boost/math/special_functions/legendre.hpp>
# include <boost/math/special_functions/bessel.hpp>
# include <boost/math/special_functions/bessel_prime.hpp>
# include <boost/math/special_functions/hankel.hpp>
#endif

template<>

InputParameters validParams<MieElecFieldImag>()

{
  InputParameters params = validParams<AuxKernel>();
  params.addRequiredParam<Real>("a", "radius of the particle");
  params.addRequiredParam<Real>("omega", "angular frequency of the plane wave");
  params.addRequiredParam<Real>("c", "the speed of light");
  params.addRequiredParam<Real>("epsilonI", "the dielectric constant of the medium");
  params.addRequiredParam<Real>("sigmaI", "the conductivity of the medium");
  params.addRequiredParam<Real>("epsilonII", "the dielectric constant of the sphere");
  params.addRequiredParam<Real>("sigmaII", "the conductivity of the sphere");
  params.addRequiredParam<Real>("L", "wavelength of the plane wave");
  params.addRequiredParam<Real>("nh", "ratio of complex index of refractions of sphere and media");
  params.addRequiredParam<Real>("order", "order of the expansions");
  params.addRequiredParam<Real>("scale", "scale");
  params.addRequiredParam<Real>("component", "component at which to compute this field ('0 for x, 1 for y, 2 for z')");
  return params;
}


MieElecFieldImag::MieElecFieldImag(const InputParameters & parameters) :
  AuxKernel(parameters),
  _a(getParam<Real>("a")),
  _omega(getParam<Real>("omega")),
  _c(getParam<Real>("c")),
  _epsilonI(getParam<Real>("epsilonI")),
  _sigmaI(getParam<Real>("sigmaI")),
  _epsilonII(getParam<Real>("epsilonII")),
  _sigmaII(getParam<Real>("sigmaII")),
  _L(getParam<Real>("L")),
  _nh(getParam<Real>("nh")),
  _order(getParam<Real>("order")),
  _scale(getParam<Real>("scale")),
  _component(getParam<Real>("component"))
{
}

Real
MieElecFieldImag::computeValue()
{
#ifdef FERRET_HAVE_BOOST_MATH_SPECIAL_FUNCTIONS
  Real x = _q_point[_qp](0);
  Real y = _q_point[_qp](1);
  Real z = _q_point[_qp](2);

  Real r =  std::pow(x, 2.0) + std::pow(y, 2.0) + std::pow(z, 2.0);
  Real th = std::acos(z / r);
  Real phi = std::atan(y / x); 
  Real q = 2 * libMesh::pi * _a / _L;
 
  Complex k1I = Complex(0,1) * (_omega / _c) * (_epsilonI + Complex(0,1) * 4.0 * libMesh::pi * _sigmaI / _omega);
  Complex k2I = Complex(0,1) * (_omega / _c);
  Complex kI =  std::sqrt(-k1I*k2I);

  Complex k1II = Complex(0,1) * (_omega / _c) * (_epsilonII + Complex(0,1) * 4.0 * libMesh::pi * _sigmaII / _omega);
  Complex k2II = Complex(0,1) * (_omega / _c);
  Complex kII =  std::sqrt(-k1II*k2II);

  Complex Ersj = 0.0;
  Complex Ethsj = 0.0;
  Complex Ephisj = 0.0;

  Complex Ex = 0.0;
  Complex Ey = 0.0;
  Complex Ez = 0.0;


  for(unsigned int j = 1; j < _order; ++j)
  {
    Complex eBj = ( (2.0 * j + 1.0) / (j *(j + 1.0 )) ) * std::pow(Complex(0,1), j + 1) * ((_nh * boost::math::sph_bessel_prime(j, q) * boost::math::sph_bessel(j, _nh * q) - boost::math::sph_bessel_prime(j, _nh * q) * boost::math::sph_bessel(j, q))/(_nh * 0.5 * (boost::math::sph_hankel_1(j - 1, q) - q * (boost::math::sph_hankel_1(j, q) + q * boost::math::sph_hankel_1(j + 1, q) ) / std::pow(q, 2.0) ) * boost::math::sph_bessel(j, _nh * q) - boost::math::sph_hankel_1(j, q) * boost::math::sph_bessel_prime(j, _nh * q)));

    Complex mBj = ( (2.0 * j + 1.0) / (j *(j + 1.0 )) ) * std::pow(Complex(0,1), j + 1) * ((_nh * boost::math::sph_bessel(j, q) * boost::math::sph_bessel_prime(j, _nh * q) - boost::math::sph_bessel_prime(j, q) * boost::math::sph_bessel(j, _nh * q))/(_nh * boost::math::sph_hankel_1(j, q) * boost::math::sph_bessel_prime(j, _nh * q) - 0.5 * (boost::math::sph_hankel_1(j - 1, q) - q * (boost::math::sph_hankel_1(j, q) + q * boost::math::sph_hankel_1(j + 1, q) ) / std::pow(q, 2.0) ) * boost::math::sph_bessel(j, _nh * q) ));

    Ersj += (std::pow(_L, 2.0) * std::cos(phi) / (4 * std::pow(libMesh::pi, 2.0) * r * r)) * j * (j + 1.0) * eBj * boost::math::sph_hankel_1(j, (2 * libMesh::pi / _L) * r) *  boost::math::legendre_p(j, 1, std::cos(th)
);

    Ethsj += - (std::pow(_L, 2.0) * std::cos(phi) / (4 * std::pow(libMesh::pi, 2.0) * r)) * (
eBj * 0.5 * (boost::math::sph_hankel_1(j - 1, (2 * libMesh::pi / _L) * r) - (2 * libMesh::pi / _L) * r * (boost::math::sph_hankel_1(j, (2 * libMesh::pi / _L) * r) + (2 * libMesh::pi / _L) * r * boost::math::sph_hankel_1(j + 1, (2 * libMesh::pi / _L) * r) ) / std::pow((2 * libMesh::pi / _L) * r, 2.0) ) * ((std::sin(th) * boost::math::legendre_p(j, 2, std::cos(th)) + std::cos(th) * boost::math::legendre_p(j, 1, std::cos(th)) )/ (std::sin(th) * std::sin(th))) * std::sin(th)
- Complex(0,1) * mBj * boost::math::sph_hankel_1(j, (2 * libMesh::pi / _L) * r) * boost::math::legendre_p(j, 1, std::cos(th)) / std::sin(th)
);

    Ephisj += - (std::pow(_L, 2.0) * std::sin(phi) / (4 * std::pow(libMesh::pi, 2.0) * r)) * (
(eBj / std::sin(th)) * 0.5 * (boost::math::sph_hankel_1(j - 1, (2 * libMesh::pi / _L) * r) - (2 * libMesh::pi / _L) * r * (boost::math::sph_hankel_1(j, (2 * libMesh::pi / _L) * r) + (2 * libMesh::pi / _L) * r * boost::math::sph_hankel_1(j + 1, (2 * libMesh::pi / _L) * r) ) / std::pow((2 * libMesh::pi / _L) * r, 2.0) ) * boost::math::legendre_p(j, 1, std::cos(th))
+ Complex(0,1) * mBj * boost::math::sph_hankel_1(j, (2 * libMesh::pi / _L) * r) * ((std::sin(th) * boost::math::legendre_p(j, 2, std::cos(th)) + std::cos(th) * boost::math::legendre_p(j, 1, std::cos(th)) )/ (std::sin(th) * std::sin(th))) * std::sin(th)
);
  }

  if (_component == 0)
  {
    Ex += Ethsj * std::cos(th) * std::cos(phi) +  Ersj * std::cos(phi) * std::sin(th) - Ephisj * std::sin(th);
    return _scale * Ex.imag();
  }
  else if (_component == 1)
  {
    Ey += Ersj * std::sin(th) * std::sin(phi) +  std::cos(phi) * Ephisj + Ethsj * std::sin(th) * std::sin(phi);
    return _scale * Ey.imag();
  }
  else
  {
    Ez += Ersj * std::cos(th) - Ethsj * std::sin(th);
    return _scale * Ez.imag();
 }
#else
  return -1.0;
#endif
}
